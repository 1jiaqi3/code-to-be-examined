#include "app.h"
#include <signal.h>
#include <stdio.h>
#include <errno.h>
#include <iostream>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <system_error>
#include <unistd.h>
#include <vector>
#include <string>
#include "PracticalSocket.h"
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include "utilities.h"
using namespace std;
void signal_handler(int signum);

// Constructor
app::app() // Note it has default values
    : virtual_memory_limit(-1),
      scheduling_policy(SCHED_OTHER),
      scheduling_priority(0) {

  // Set the signal_handler in ctor to ensure that the function
  // would be executed when we do receive a signal. Not just in execute()
  struct sigaction sa;
  sa.sa_handler = &signal_handler;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART | SA_NOCLDSTOP;
  // Whenever the child is terminated, the parent is sent a SIGCHID signal
  if (sigaction(SIGCHLD, &sa, 0) == -1) {
    perror(0);
    exit(1);
  }

  cerr << "===============================================" << endl;
  cerr << "Welcome to the CS3281 Shell Assignment " << endl;
  cerr << "===============================================" << endl;
}

// Destructor
app::~app() {

  cerr << "===============================================" << endl;
  cerr << "Closing CS3281 Shell Assignment " << endl;
  cerr << "===============================================" << endl;
}

// Handle the command that has "||" delimiters
int app::parallel_execution(std::string command_string, int sock_fd) {
  int saved_stdout = dup(1);
  dup2(sock_fd, 1);

  std::vector<string> parallel_commands;
  std::vector<int> children;            // Store pid
  std::vector<string> childrenCommands; // Store command
  // Separated by "||"
  tokenize_string(command_string, parallel_commands);

  // Execute all the commands in parallel
  for (auto command_str : parallel_commands) {
    LOG << "Launch " << command_str << endl;
    std::vector<string> command;
    // Separated by " "
    tokenize_string(command_str, command, " ");

    if (checkforeground(command)) {
      LOG << command[0] << " is foreground" << endl;
      if (checkbuiltin(command)) {
        executebuiltin(command, sock_fd);
      } else {
        // Execute as background task and store pid in pidlist
        // Need to execute with false parameter. Else you are not running parallelly.
        // Don't understand...?
        int pid = execute(command, false, sock_fd);
        children.push_back(pid);
        childrenCommands.push_back(command[0]);
      }
    } else {
      LOG << command[0] << " is background" << endl;
      if (checkbuiltin(command)) {
        executebuiltin(command, sock_fd);
      } else {
        execute(command, false, sock_fd);
      }
    }
    // @Task 1: Launch the parallel commands - note that if command1 ||
    // command2
    // is given, you must launch both of them and then wait for both of them.
    // make sure to check if they are built in and if they should not be
    // waited upon i.e. if they are not foreground tasks. Use checkforeground
    // function for that.
  }

  for (auto child : children) {
    int status = 0;
    int retvalue = waitpid(child, &status, 0);
    if (retvalue < 0) {
      LOG << "error occurred " << strerror(errno)
          << "Did we already wait for it in the signal handler.\n";
      continue;
    } else {
      if (WIFEXITED(status)) {
        int exitcode = WEXITSTATUS(status);
        if (exitcode == 0)
          LOG << childrenCommands[child] << ": Child Exited Successfully\n";
        else
          LOG << childrenCommands[child]
              << ": Child Self Terminated With Exit Code " << exitcode << "\n";
      } else if (WIFSIGNALED(status)) {
        int signalsent = WTERMSIG(status);
        LOG << childrenCommands[child] << ": Child Terminated Due to Signal "
            << signalsent << "\n";
      }
    }
  }
  dup2(saved_stdout, 1);
  return 0;
}

// If commands in checkbuiltin, then invoke executbuiltin()
bool checkbuiltin(std::vector<std::string> &command) {
  //@ Task2: complete this check to include other built in commands - see
  // readme.
  if (command[0] == "set_memlimit" || command[0] == "cd" ||
      command[0] == "set_policy" || command[0] == "set_priority")
    return true;
  else
    return false;
}

// A built in is a command that changes the properties of
// the parent shell itself and does not create a new process.
int app::executebuiltin(std::vector<string> &command, int sock_fd) {
  int saved_stdout = dup(1);
  dup2(sock_fd, 1);
  
  // all built in command have 1 command and 1 argument. The last entry is
  // return.

  if (command.size() != 2) {
    LOG << "Built in commands require two arguments\n";
    return -1;
  }

  if (command[0] == "cd" && command.size() >= 2) {
    // @Task 3: Implement the command to change directory. Search for chdir
    // c_str returns a pointer to an array/vector that contains a
    // null-terminated sequence of characters
    const char *directory = command[1].c_str();
    int ret;
    if (*directory == '~' || *directory == ' ' || directory == nullptr) {
      char *home = getenv("HOME");
      ret = chdir(home);
    }
    ret = chdir(directory);
    if (ret < 0) {
      LOG << "Change directory failed " << errno << "\n";
    } else {
      LOG << "Got a command to change directory to " << command[1] << std::endl;
    }
  } else if (command[0] == "set_memlimit") {
    std::cout << "setting memlimit to " << command[1] << " bytes\n";
    try {
      int bytes = std::stoi(command[1]); // stoi: string to int
      this->virtual_memory_limit = bytes;
    } catch (...) {
      LOG << "Exception occured while converting " << command[1] << " to int\n";
    }
  } else if (command[0] == "set_policy") {
    LOG << "setting policy to " << command[1] << "\n";
    if (command[1] == "fifo") {
      this->scheduling_policy = SCHED_FIFO;
    } else if (command[1] == "rr") {
      this->scheduling_policy = SCHED_RR;
    } else if (command[1] == "other") {
      this->scheduling_policy = SCHED_OTHER;
    } else {
      LOG << "wrong policy " << command[1] << std::endl;
    }
  } else if (command[0] == "set_priority") {
    std::cout << "setting priority to " << command[1] << "\n";
    try {
      int priority = std::stoi(command[1]);
      if ((this->scheduling_policy == SCHED_FIFO ||
           this->scheduling_policy == SCHED_RR)) {
        if (priority >= 1 && priority <= 99) {
          this->scheduling_priority = priority;
        } else {
          LOG << "real time priority should be between 1 and 99\n";
        }
      } else if (priority >= -20 && priority <= 19) {
        this->scheduling_priority = priority;
      } else {
        LOG << "niceness should be between -20 and 19\n";
      }
    } catch (...) {
      LOG << "Exception occured while converting " << command[1] << " to int\n";
    }
  }
  // @Task 3: Implement other builtins as specified in the readme.
  dup2(saved_stdout, 1);
}

void writehere(char *msg) { write(1, msg, strlen(msg)); }

// async-safe implementation
void positive_integer_to_string(int number, char *buffer, int length) {
  // count number of digits
  int numdigits, num;
  numdigits = 0;
  num = number;
  while (num != 0) {
    numdigits++;
    num /= 10;
  }
  if (length < (numdigits + 1))
    return;
  int remainder, i;

  for (i = 0; i < numdigits; i++) {
    remainder = number % 10;
    number = number / 10;
    buffer[numdigits - (i + 1)] = remainder + '0';
  }
  buffer[numdigits] = '\0';
}

// @Task 4: Implement the SIGCHLD signal handler.
// Read https://www.win.tue.nl/~aeb/linux/lk/lk-5.html first
// It has a great explaination to all the components of "Signal".
// Then read
// http://www.microhowto.info/howto/reap_zombie_processes_using_a_sigchld_handler.html#idp67312
// To have a detailed understanding of the mechanism.
// Simplest dead child cleanup in a SIGCHLD handler. Prevent zombie processes
void signal_handler(int signum) {
  int status = 0;
  // Wait for any terminated child process
  int child_pid = 1;
  while (child_pid > 0) {
    // As soon as the child's status has been reported, the zombie vanishes
    child_pid = waitpid((pid_t)(-1), 0, WNOHANG);
    if (child_pid < 0) {
      char buffer[256];
      strerror_r(errno, buffer, 256);
      write(1, buffer, strlen(buffer));
      write(1, "\n", 1);
      return;
    } else {
      if (WIFEXITED(status)) {
        writehere("exited status ");
        int exitcode = WEXITSTATUS(status);
        char exitstring[20];
        exitstring[0] = '\0';
        positive_integer_to_string(exitcode, exitstring, 20);
        writehere(exitstring);
        writehere("\n");
      } else if (WIFSIGNALED(status)) {
        writehere("terminated by signal ");
        int termsig = WTERMSIG(status);
        char termsigstring[20];
        termsigstring[0] = '\0';
        positive_integer_to_string(termsig, termsigstring, 20);
        writehere(termsigstring);
        writehere("\n");
      }
    }
  }
}

// This function is going to execute the shell command and going to execute
// wait, if the second parameter is true;
int app::execute(std::vector<string> &command, bool executingforeground, int sock_fd) {
  int saved_stdout = dup(1);
  dup2(sock_fd, 1);
  
  pid_t w;
  int status;

  // Command string can contain the main command and a number of command line
  // arguments. We should allocate one extra element to have space for null.

  int commandLen = command.size();

  // If executing in background, remove "&" from command list passed to execvp
  if (!executingforeground) {
    commandLen--;
  }

  // Create the pointer that point to the pointers which point to the commands
  // We should allocate one extra element to have space for null
  char **args = (char **)malloc((commandLen + 1) * sizeof(char *));
  for (int i = 0; i < commandLen; i++) {
    args[i] = strdup(command[i].c_str());
  }
  args[command.size()] = 0;

  // create a new process
  w = fork();
  if (w < 0) {
    LOG << "\nFork Failed " << errno << "\n";
    return 0;
  } else if (w == 0) { // Child
    // @Task 5: Use the API to implement the memory limits, scheduling policy
    // and scheduling priority.
    // First "if" just check the member variables that defined by ourself
    // member variables (virtual_memory_limit, scheduling_policy,
    // scheduling_priority)
    // see if their current value is setted by invoking executebuiltin(command)
    // function
    // While in real execution in the child process, we need to use the API to
    // really set
    // these variables' value (process's characters) into system
    if (virtual_memory_limit > 0) {
      struct rlimit rl;
      // Get the limit on memory
      getrlimit(RLIMIT_AS, &rl);
      printf("\n Default virtual memory is : %d\n", (int)rl.rlim_cur);

      // Change the limit
      rl.rlim_cur = (rlim_t)virtual_memory_limit;
      rl.rlim_max = (rlim_t)virtual_memory_limit;

      // Now call setrlimit() to set the changed value.
      int retvalue = setrlimit(RLIMIT_AS, &rl);
      if (retvalue < 0) {
        std::cerr << "error occured " << strerror(errno)
                  << " . Try running with sudo\n";
        exit(2);
      }

      // Again get the limit and check
      getrlimit(RLIMIT_AS, &rl);
      printf("\n The virtual memory of process is : %d\n", (int)rl.rlim_cur);
    }
    if (scheduling_policy != SCHED_BATCH && scheduling_policy != SCHED_IDLE) {
      struct sched_param sp;
      sp.sched_priority = scheduling_priority;

      // Check if the scheduling policy is compatible with the priority
      int upperBound = sched_get_priority_max(scheduling_policy),
          lowerBound = sched_get_priority_min(scheduling_policy);
      if (scheduling_priority < lowerBound ||
          scheduling_priority > upperBound) {
        std::cerr << "error occured " << strerror(errno)
                  << "The scheduling policy is incompatible with the "
                     "priority\n";
        exit(2);
      }

      int retvalue = sched_setscheduler(0, scheduling_policy, &sp);
      if (retvalue < 0) {
        std::cerr << "error occured " << strerror(errno)
                  << " . Try running with sudo\n";
        exit(2);
      }
    }

    // Here is the real execute!
    LOG << "Going to exec " << args[0] << "\n";
    execvp(args[0], args);
    LOG << "\nExec Failed " << errno << "\n";
    exit(2);
  } else // Parent
  {

    // return the child pid if we are not waiting in foreground
    if (!executingforeground) {
      dup2(saved_stdout, 1);
      return w;
    }

    int status;
    int retvalue = 0;
    while (retvalue != w) {
      status = 0;
      retvalue = waitpid(w, &status, 0);
      if (retvalue < 0) {
        char buffer[256];
        strerror_r(errno, buffer, 256);
        printf("error occured %s\n", buffer);
        break;
      } else {
        if (WIFEXITED(status)) {
          int exitcode = WEXITSTATUS(status);
          if (exitcode == 0)
            LOG << args[0] << ":Child Exited Successfully\n";
          else
            LOG << args[0] << ": Child Self Terminated With Exit Code "
                << exitcode << "\n";
        } else if (WIFSIGNALED(status)) {
          int signalsent = WTERMSIG(status);
          LOG << args[0] << ": Child Terminated Due to Signal " << signalsent
              << "\n";
        }
      }
    }
  }
  dup2(saved_stdout, 1);
  return 0;
}
