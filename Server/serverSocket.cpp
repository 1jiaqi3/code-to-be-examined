#include "PracticalSocket.h"  // For Socket, ServerSocket, and SocketException
#include <iostream>           // For cerr and cout
#include <cstdlib>            // For atoi()
#include<string.h>
#include <signal.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
using namespace std;

const unsigned int RCVBUFSIZE = 32;    // Size of receive buffer


int main(int argc, char *argv[]) {
  if (argc != 2) {                     // Test for correct number of arguments
    cerr << "Usage: " << argv[0] << " <Server Port>" << endl;
    exit(1);
  }
  unsigned short servPort = atoi(argv[1]);  // First arg: local port

  try {
    TCPServerSocket servSock(servPort);     // Server Socket object
    cout << "Hi! I am a server!" << endl;
    for (;;) {   // Run forever
      // Wait for a client to connect
      TCPSocket* csock;
      csock = servSock.accept();
      int sockDesc = csock->getSocketDesc();
      cout << "Handling client ";
      try {
	cout << csock->getForeignAddress() << ":";
      } catch (SocketException e) {
	cerr << "Unable to get foreign address" << endl;
      }
      try {
	cout << csock->getForeignPort();
      } catch (SocketException e) {
	cerr << "Unable to get foreign port" << endl;
      }
      cout << endl;

      pid_t pid = fork();
      if (pid < 0) {
	std::cout << "\nFork Failed " << errno << "\n";
	exit(1);
      } 
      else if (pid == 0){
	dup2(sockDesc, 0);
	dup2(sockDesc, 1);
	dup2(sockDesc, 2);
	execl("/home/vagrant/281projects/Jiaqi-Hengqing-Project/Server/shell", "shell", (char*)NULL);
	std::cout<<"error occured\n";
      }
    }
  } catch (SocketException &e) {
    cerr << e.what() << endl;
    exit(1);
  }
  // NOT REACHED

  return 0;
}
