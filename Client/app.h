#ifndef APP_H
#define APP_H

#include <iostream>
#include <sched.h>
#include <vector>
#include <string>
#include <map>
#include <signal.h>
#include "PracticalSocket.h"
class app {
  int execute(std::vector<std::string> &str, bool executingforeground, int sock_fd);
  int parallel_execution(std::string command_string, int sock_fd);
  int executebuiltin(std::vector<std::string> &command, int sock_fd);
  // Here is the declaration of this three variable
  int virtual_memory_limit;
  int scheduling_policy;
  int scheduling_priority;
  // Helper function. why cannot declare here?
  // void signal_handler (int signum);

public:
  app();
  ~app();
  void start(int argc, char* argv[]);
  void HandleTCPClient(TCPSocket *sock);
};

#endif // APP_H
