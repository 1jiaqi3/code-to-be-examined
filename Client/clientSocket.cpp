#include "app.h"
#include <memory>
#include "utilities.h"
#include <vector>
#include <string>
#include "PracticalSocket.h"  // For Socket and SocketException
#include <iostream>           // For cerr and cout
#include <cstdlib>            // For atoi() 

const int RCVBUFSIZE = 32;
using namespace std;
int main (int argc, char *argv[])
{
  string servAddress = argv[1]; // First arg: server address
  unsigned short servPort = (argc == 3) ? atoi(argv[2]) : 20000;
  try {
    // Establish connection with the echo server
    TCPSocket sock(servAddress, servPort); 
    // Send the string to the echo server
    char buffer[RCVBUFSIZE + 1]; // Buffer for echo string + \0
    int bytesReceived = 0;             // Bytes read on each recv()
    // Receive the same string back from the server
      string command_str;
      getline (cin, command_str);


    while(command_str!="exit"){
      int commandLen = command_str.size();
      char *cstr = new char[commandLen + 1];
      strcpy(cstr, command_str.c_str());
      // Send the string to the echo server
      sock.send(cstr, commandLen);
      while (1){
	memset(buffer, 0, RCVBUFSIZE + 1);
	// Receive up to the buffer size bytes from the sender
	if ((bytesReceived = (sock.recv (buffer, RCVBUFSIZE))) < 0)
	  {
	    break;
	  }
      //echoBuffer[bytesReceived] = '\0';    // Terminate the string!
	cout << buffer;                  // Print the echo buffer
      }
      getline(cin,command_str);
     
    }
  }             // Setup to print the received output
  catch(SocketException &e) {
    cerr << e.what() << endl;
    exit(1);
  }
}
