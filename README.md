# Jiaqi-Hengqing-Project

Note 1: UDP is enough here, we can use datagram socket type.

Question 1: Here we create new processes for new coming client instead of using threads. what the reason?
Does it avoid the race condition/dead lock?

Question 2: Does INADDR_ANY will figure out what the actual IP address is when running?

*Question 3: Can UDP socket also use accept() system call when a new client comes, which we can then fork()?
How to achieve concurrency in the UDP server?

*Question 4: The file description returned from socket() can read and write? which means we can redirect the input 
commands from the client and the output info after executing in the server both into this file description?
Still not clear about file description?? 

2016/04/16:
I think we can make use of the example socket code. It has a good abstraction of system calls, and with good error handling. 


